# nginx-f5n-exam-prep

Made this repo to gather information that allows me to prep a little before doing the F5N2 and F5N4 exams. I have already passed F5N1 and F5N3 and just wanted to have a place where I could share some prep notes on the exam.

More information on the exams can be found here: 

- [NGINX Management (F5N1)](https://education.f5.com/hc/en-us/articles/21216088175003-NGINX-Management-F5N1-)
- [NGINX Configuration: Knowledge (F5N2)](https://education.f5.com/hc/en-us/articles/21216490361627)
- [NGINX Configuration: Demonstrate (F5N3)](https://education.f5.com/hc/en-us/articles/21216540419611)
- [NGINX Troubleshoot (F5N4)](https://education.f5.com/hc/en-us/articles/21216598693275)

Badge you are able to earn: [F5 Certified! Administrator](https://www.credly.com/org/f5/badge/f5-certified-administrator-nginx-f5-ca-nginx.1)

# NGINX F5N2 Configuration: Knowledge Exam Preparation

This guide covers the key concepts and configurations you'll need to master for the "NGINX Configuration: Knowledge" exam.



## Section A: Configure NGINX as a Load Balancer



* **A1: Define the load balancing pools/systems:**

  * Load balancing pools, also known as upstream groups, are defined within the `http` block of the NGINX configuration using the `upstream` directive. They contain the addresses (and optionally, parameters) of the backend servers that NGINX distributes traffic to.

  * **Example:**



```
  nginx

  upstream my_backend {

    server backend1.example.com;   # Simple server entry

    server backend2.example.com:8080; # Specify a different port

    server 192.168.1.100 max_fails=3; # Retry 3 times before marking down

  }

```



* **A2: Explain the different load balancing algorithms:**

  * NGINX offers several algorithms for distributing traffic among the servers in an upstream group:

    * **Round Robin (default):** Each request is assigned to the next server in a sequential manner.

    * **Least Connections:** Assigns the request to the server with the fewest active connections.

    * **IP Hash:** Uses the client's IP address to consistently direct the client to the same server.

    * **Weighted:** Assigns weights to servers, directing a proportional amount of traffic based on those weights. (See the `weight` parameter in the example above.)



* **A3: Describe the process used to remove a server from the pool:**

  * **Temporarily:** Add the `down` parameter to the server line in the `upstream` block. This will prevent NGINX from sending traffic to that server.

  * **Permanently:** Remove the entire server line from the `upstream` block and reload the NGINX configuration (`nginx -s reload`).



* **A4: Describe what happens when a pool server goes down:**

  * NGINX actively monitors the health of backend servers using health checks. When a server fails, it's marked as `down`. NGINX will stop sending requests to it and automatically redirect traffic to the remaining healthy servers.



* **A5: Explain what is unique to NGINX as a load balancer:**

  * **Event-driven Architecture:** NGINX uses an asynchronous, event-driven approach, making it highly efficient and scalable under heavy load.

  * **Flexibility (Layer 4 and Layer 7):** NGINX can operate as both a Layer 4 (transport layer) load balancer (using the `stream` directive) and a Layer 7 (application layer) load balancer (using the `http` directive). This allows for a wide range of load balancing strategies.

  * **Rich Feature Set:** NGINX offers advanced features like session persistence, request buffering, and support for various protocols (HTTP, HTTPS, WebSocket, gRPC, etc.).



* **A6: Describe how to configure security:**

  * NGINX provides multiple layers of security:

    * **SSL/TLS Termination:** Encrypt traffic between clients and NGINX. (See example in Section C1)

    * **Access Control:** Use `allow` and `deny` directives to restrict access to specific IP addresses or ranges.

    * **HTTP Authentication:** Require users to authenticate before accessing protected resources.

    * **Web Application Firewall (WAF):** NGINX Plus offers a WAF module to protect against common web attacks.



* **A7: Modify or tune a memory zone configuration:**

  * Memory zones are shared memory areas used by NGINX for various purposes (e.g., caching, session persistence). You can adjust their size using the `zone` directive in the `http` context.

  * **Example:**



```
  nginx

  http {

    proxy_cache_path /data/nginx/cache levels=1:2 keys_zone=my_cache:10m;

  }
```



* **A8: Describe how to configure NGINX as a mirroring server:**

  * The `mirror` directive sends a copy of the original request to a specified server (often a logging or monitoring server) without affecting the client's original request.

  * **Example:**



```
  nginx

  location / {

    mirror /mirror;

    proxy_pass http://my_backend;

  }



  location /mirror {

    internal;

    proxy_pass http://mirror_server;

  }

```



* **A9: Describe how to configure NGINX as a layer 4 load balancer:**

  * NGINX can load balance TCP and UDP traffic using the `stream` module.

  * **Example:**



```
  nginx

  stream {

    upstream tcp_servers {

      server backend1.example.com:1234;

      server backend2.example.com:1234;

    }



    server {

      listen 1234;

      proxy_pass tcp_servers;

    }

  }

```



* **A10: Describe how to configure NGINX as an API Gateway:**

  * NGINX can act as a powerful API gateway, handling routing, authentication, rate limiting, and other API management tasks. 

  * **Example (Simple Routing):**



```
  nginx

  location /api/v1/users {

    proxy_pass http://user_service;

  }



  location /api/v1/products {

    proxy_pass http://product_service;

  }

```

## Section B: Configure NGINX as a Content Cache Server



* **B1: Define a minimum retention policy:**

  * A minimum retention policy ensures that cached content is not evicted from the cache prematurely. You can set this using the `proxy_cache_min_uses`       directive, which specifies the minimum number of times a cached response must be used before it can be considered for eviction.

  * **Example:**

```
nginx

  proxy_cache_path /data/nginx/cache levels=1:2 keys_zone=my_cache:10m inactive=60m;

  proxy_cache my_cache;

  proxy_cache_min_uses 3; # Cache must be used 3 times before eviction

```



* **B2: Describe how to configure path regex routing:**

  * You can use regular expressions (regex) within `location` blocks to match specific URI patterns and apply caching rules selectively.

  * **Example:**

```
nginx

  location ~* \.(jpg|jpeg|png|gif|ico|css|js)$ {

    proxy_cache my_cache;

    proxy_cache_valid 200 302 1h; # Cache for 1 hour

    proxy_cache_valid 404 1m;   # Cache 404 errors for 1 minute

  }

```



* **B3: Describe the why and how of caching in NGINX:**

  * **Why:**

    * Reduce the load on backend servers.

    * Improve response times for clients, especially for frequently accessed static content.

    * Save bandwidth by serving cached content directly from NGINX.

  * **How:**

    * NGINX stores cached responses in a configurable location (file system or memory).

    * It caches based on a combination of URI, request headers, and other criteria.

    * When a request arrives, NGINX checks if a matching cached response exists and serves it if it does.



* **B4: Define the cache in the http context:**

  * The `proxy_cache_path` directive in the `http` context defines the location, size, and other parameters of the cache.

  * **Example:**

```
nginx

  http {

    proxy_cache_path /data/nginx/cache levels=1:2 keys_zone=my_cache:10m max_size=1g;

  }

```

  * In this example:

    * `/data/nginx/cache` is the cache directory.

    * `levels=1:2` creates a two-level directory structure for storing cached files.

    * `keys_zone=my_cache:10m` sets up a shared memory zone (`my_cache`) for storing cache keys and metadata (10MB in size).

    * `max_size=1g` limits the cache size to 1GB.



* **B5: Enable the cache:**

  * The `proxy_cache` directive is used within a `location` block to enable caching for that particular location.

  * **Example:**

```
nginx

  location /images/ {

    proxy_cache my_cache;

    proxy_pass http://backend_servers;

  }

```



* **B6: Specify the content that should be cached:**

  * The `proxy_cache_valid` directive defines how long different types of responses should be cached. 

  * **Example:**

```
nginx

  proxy_cache_valid 200 301 302 1h; # Cache successful responses and redirects for 1 hour

  proxy_cache_valid any 5m;      # Cache other responses for 5 minutes

```



* **B7: Describe different types of caching:**

  * **Proxy Caching:** Caching responses from backend servers (as described above).

  * **FastCGI Caching:** Caching responses from FastCGI applications like PHP-FPM.

  * **Memcached Caching:** Using Memcached as a distributed caching backend for NGINX.



* **B8: Explain what is unique to NGINX as a cache server:**

  * **High Performance:** NGINX's architecture allows for very efficient caching, even under heavy load.

  * **Fine-Grained Control:** NGINX offers a wide range of directives to control caching behavior.

  * **Integration with Other Features:** NGINX caching integrates seamlessly with its other features like load balancing and SSL/TLS termination.



## Section C: Configure NGINX as a Web Server



* **C1: Demonstrate how to securely serve content (HTTP/HTTPS):**



  * **HTTP Configuration:**



```
nginx

  server {

    listen 80;     # Listen on port 80 (default HTTP port)

    server_name example.com www.example.com; # Specify the domain names



    location / {

      root /var/www/html; # Root directory for website files

      index index.html;  # Default file to serve

    }

  }

```

  * **HTTPS Configuration:**

```
nginx

  server {

    listen 443 ssl; # Listen on port 443 (default HTTPS port)

    server_name example.com www.example.com;



    ssl_certificate /etc/ssl/certs/example.com.crt; # Path to your certificate

    ssl_certificate_key /etc/ssl/private/example.com.key; # Path to your private key



    location / {

      root /var/www/html;

      index index.html;

    }

  }

```



* **C2: Describe the difference between serving static content and dynamic content (REGEX, and variables):**

  * **Static Content:** Files like HTML, CSS, JavaScript, and images that are served directly from the file system without any processing.

  * **Dynamic Content:** Generated on the fly by applications (e.g., PHP, Python, Ruby) in response to requests.

  * **REGEX (Regular Expressions):** Used in `location` blocks to match specific URI patterns.

  * **Variables:** Allow dynamic configuration and content generation.

  * **Example (Serving Dynamic Content with PHP):**



```
nginx

  location ~ \.php$ {

    fastcgi_pass unix:/run/php/php7.4-fpm.sock;

    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;

    include fastcgi_params;

  }

```



* **C3: Describe how server and location work:**

  * **`server` Block:** Defines a virtual server that handles requests for specific domain names or IP addresses.

  * **`location` Block:** Nested within a `server` block, it specifies how NGINX should handle requests for different parts of your website (URIs). 

  * **Example:**



```
nginx

  server {

    # ... server configuration ...



    location / {

      # Rules for the root path ("/")

    }



    location /images/ {

      # Rules for URIs starting with "/images/"

    }

  }

```



* **C4: Explain what is unique to NGINX as a web server:**

  * **Asynchronous Architecture:** Handles many connections concurrently with minimal resources.

  * **Reverse Proxy Functionality:** Can act as a gateway to other web servers.

  * **Lightweight:** Low memory footprint and efficient resource utilization.

  * **Flexibility:** Highly configurable for various use cases.



## Section D: Configure NGINX as a Reverse Proxy



* **D1: Explain how traffic routing is handled in NGINX as a reverse proxy:**



  * NGINX receives requests from clients and forwards them to the appropriate backend server based on the `location` configuration. It then sends the response from the backend server back to the client.



  * **Example:**



```
nginx

  location /api/ {

    proxy_pass http://backend_api_servers/; # Route requests to backend API servers

  }

```



* **D2: Explain what is unique to NGINX as a reverse proxy:**

  * **High Performance:** Efficiently handles a large number of concurrent connections.

  * **Load Balancing:** Distributes traffic across multiple backend servers.

  * **Caching:** Caches responses from backend servers to improve performance.

  * **SSL/TLS Termination:** Offloads SSL/TLS decryption from backend servers.

  * **Header Manipulation:** Modifies request and response headers.

  * **Web Application Firewall (WAF):** (Available in NGINX Plus) Adds a layer of security.



* **D3: Configure encryption:**

  * See the HTTPS configuration in section C1.



* **D4: Demonstrate how to manipulate headers:**

  * **`proxy_set_header`:** Modifies or adds headers to the request sent to the backend server.

  * **`add_header`:** Adds headers to the response sent back to the client.

  * **Example:**



```
nginx

  location /api/ {

    proxy_set_header Host $host;      # Preserve original Host header

    proxy_set_header X-Real-IP $remote_addr; # Add client's real IP



    add_header X-Cache-Status $upstream_cache_status; # Add cache status to response

    proxy_pass http://backend_api_servers/; 

  }

```



* **D5: Describe the difference between proxy_set_header and add_header:**

  * As mentioned above, `proxy_set_header` works on requests sent to the backend, while `add_header` works on responses sent back to the client.



* **D6: Modify or tune a memory zone configuration:**

  * See section A7.



* **D7: Describe how to configure NGINX as a socket reserve proxy:**

  * You can use `proxy_pass` to forward requests to a Unix domain socket instead of a TCP/IP address.

  * **Example:**



```
nginx

  location / {

    proxy_pass http://unix:/tmp/backend.sock:/; # Proxy to Unix socket

  }

```





* **D8: Describe how open source NGINX handles health checks in different situations:**

  * **Active Health Checks:** NGINX periodically sends specific requests (e.g., TCP connection, HTTP request) to backend servers to check their availability.

  * **Passive Health Checks:** NGINX monitors the responses from backend servers to actual client requests and marks servers as unhealthy if they consistently return errors or timeouts.


# NGINX F5N4: Troubleshooting Exam Preparation

This guide covers the key concepts and configurations you'll need to master for the "NGINX Troubleshooting" exam.

## Section A: Demonstrate how to stop, start, and reload NGINX binary



 * **A1: Describe how to send signals to the NGINX process**

  * NGINX is managed using signals sent to its master process. 

  * Key signals:

   * `nginx -s stop` (immediate stop): Forcibly terminates all NGINX worker processes. Use with caution in production.

   * `nginx -s quit` (graceful shutdown): Allows current requests to finish processing before stopping worker processes. Preferred method for stopping NGINX gracefully.

   * `nginx -s reload` (reload configuration without downtime): Instructs NGINX to check the configuration file for changes and apply them if valid. New worker processes are started with the updated configuration, while old workers continue handling existing requests until they complete. 

   * `nginx -s reopen` (reopen log files): Tells NGINX to rotate and reopen log files. Useful for implementing log rotation without restarting NGINX.



   * **Example:** To reload the NGINX configuration, run the following command (replace `/usr/sbin/nginx` with the actual path to your NGINX executable):

```
bash

    sudo /usr/sbin/nginx -s reload

```


 * **A2: Describe the difference between a reload and a stop/start**



  * **Reload:** NGINX gracefully restarts worker processes after reading the new configuration. Existing connections are handled by old workers until they complete, ensuring zero downtime for users. This is the preferred method for applying configuration changes in production environments.

  * **Stop/Start:** The master process shuts down all worker processes and then restarts them. This may lead to brief service interruption as connections are closed. This is typically used when a reload is not sufficient (e.g., when changing NGINX binary or underlying system libraries).



* **A3: Describe how to test a new configuration before applying it**



  * Use the `-t` option: `nginx -t` will check the syntax of your configuration file. If there are any errors, NGINX will output error messages describing the issue. 

   * **Example:** 

```
bash

    sudo /usr/sbin/nginx -t

```

  * Use a staging environment: Ideally, create a replica of your production environment and test configuration changes there. This allows you to validate the changes without risking downtime on your live server.



## Section B: Troubleshoot basic use cases

  

 * **B1: Interpret logs**

 * **Location and Format**:

   * `access.log` (typically in `/var/log/nginx/`): Records each client request with information like the client IP, requested URL, HTTP status code, bytes sent, referrer, user agent, etc.

   * `error.log` (typically in `/var/log/nginx/`): Logs NGINX errors, including configuration issues, permission problems, or upstream server failures.  

   * NGINX log format is highly customizable. The default "combined" format includes:

```
    log_format combined '$remote_addr - $remote_user [$time_local] "$request" '

              '$status $body_bytes_sent "$http_referer" '

              '"$http_user_agent" "$http_x_forwarded_for"';
```

 * **Log Analysis Tools:**

   * `grep`: Search for specific patterns in logs (e.g., `grep " 404 " access.log` to find 404 errors).

   * `awk`: Filter and transform log data (e.g., `awk '{print $7}' access.log` to extract requested URLs).

   * `tail -f`: Monitor logs in real-time.

   * Log management systems like ELK Stack or Splunk can provide advanced search, visualization, and alerting capabilities


 * **B2: Identify start up failures**



  * **Check error.log:** The most immediate place to look for startup issues is the `error.log` file. Common errors include:

   * **Port conflicts:** Another process may already be using the port that NGINX is configured to listen on (e.g., port 80). You can use `netstat -tulpn` or `ss -tulpn` to identify processes using specific ports.

   * **Incorrect file permissions:** Ensure that the NGINX user has the necessary permissions to read the configuration file, access log files, and execute the binary.

   * **Issues with configuration syntax:** Typos, missing semicolons, or invalid directives in the configuration file can prevent NGINX from starting. Use `nginx -t` to validate the syntax before attempting to start the server. 

   * **Example (error.log entry):** 

```

    2024/05/20 08:00:05 [emerg] 12345#12345: bind() to 0.0.0.0:80 failed (98: Address already in use)

```

    This indicates that another process is already using port 80.



 * **B3: Describe how to deal with HTTP error codes**



  * **Common Error Codes and Causes:**

   * **404 (Not Found):** The requested resource does not exist on the server. Check the URL, file permissions, and NGINX configuration (location blocks).

   * **500 (Internal Server Error):** A generic error indicating an issue with the server-side application. Check the application logs for details.

   * **502 (Bad Gateway):** NGINX received an invalid response from an upstream server (e.g., PHP-FPM, Apache). Check upstream server logs and configuration.

   * **503 (Service Unavailable):** The server is temporarily unable to handle the request, often due to overload or maintenance. Check NGINX configuration for limits (`worker_connections`, `worker_processes`) and monitor server resources.

  * **Custom Error Pages:**

   * Use the `error_page` directive to define custom error pages for specific HTTP status codes. This can improve user experience and provide more informative messages.

   * **Example:**

```
nginx

    error_page 404 /404.html;

```



 * **B4: Describe how to troubleshoot various response**

  * **Incorrect Content Type:** Ensure that the `Content-Type` header in the HTTP response matches the actual content being served (e.g., `text/html` for HTML files, `application/json` for JSON data).

  * **Caching Issues:** Check cache-related headers like `Cache-Control`, `Expires`, and `ETag`. Use tools like curl or developer tools in your web browser to inspect response headers and verify caching behavior.

  * **Incorrect Location Blocks:** Verify that your NGINX configuration is correctly mapping URLs to the intended files or directories using location blocks. Ensure that there are no conflicting or overlapping location blocks. 

  * **Permission Issues:** Ensure that NGINX has the necessary permissions to read the files or directories it is trying to serve. Check file permissions using `ls -l`.



 * **B5: Describe how to troubleshoot use cases with multiple virtual hosts, multiple ports, and default servers**

  * **Multiple Virtual Hosts:** Ensure each virtual host (or server block) in your NGINX configuration file has a unique `server_name` directive that matches the domain name or IP address it should respond to. Use tools like `curl -H "Host: yourdomain.com"` to test requests against specific virtual hosts.

  * **Multiple Ports:** NGINX can listen on multiple ports simultaneously using the `listen` directive. Ensure that each `listen` directive has a unique port number. For example:

```
nginx

   server {

    listen 80;

    listen 443 ssl;

    server_name yourdomain.com;

    # ... (rest of configuration)

   }

```

 * **Default Server:** The first server block in your NGINX configuration file is typically the default server, which handles requests that don't match any specific `server_name`. You can explicitly mark a server block as the default by adding the `default_server` parameter to the `listen` directive.


 * **B6: Describe how to troubleshoot location precedence and `add_header` inheritance**



   * **Location Precedence:** In NGINX, location blocks are evaluated in order from most specific to least specific. The most specific location block that matches the request URI is used. To troubleshoot:

   * **Check the order of location blocks:** Ensure that more specific location blocks are defined before more general ones.

   * **Use the `^~` modifier:** The `^~` modifier prevents further matching of regular expressions if the prefix match is found.

   * **Example:**



```
nginx

     location ^~ /images/ { 

      # This location will match all requests starting with "/images/"

     }



     location ~ \.(gif|jpg|jpeg)$ { 

      # This location will only match if the previous block did not match

     }

```



  * **`add_header` inheritance:** By default, headers added using the `add_header` directive in a parent block are inherited by child blocks unless overridden. To troubleshoot:

    * **Check for overrides:** Ensure that a child block is not explicitly setting the same header with a different value.

    * **Use the `always` parameter:** The `always` parameter forces the header to be added even if it's already present in the response. 

    * **Example:**



```
nginx

     location / {

       add_header X-Frame-Options "SAMEORIGIN";

     }



     location /special/ {

       add_header X-Frame-Options "DENY" always; # Overrides parent header

     }

```



* **B7: Describe how to troubleshoot client and server connections**



  * **Monitoring Tools:**

   * **`netstat`, `ss`, or `lsof`:** These tools display active network connections. Use them to check the status of client and server connections. For example:

```
bash

     sudo netstat -tulpn | grep nginx
```

  * **Timeouts:** Check the NGINX configuration for relevant timeout settings:

   * **`keepalive_timeout`:** How long to keep a client connection alive for subsequent requests.

   * **`client_body_timeout`:** Maximum time to wait for the client to send the request body.

   * **`proxy_connect_timeout`, `proxy_read_timeout`, `proxy_send_timeout`:** Timeouts for upstream connections (if you're using NGINX as a reverse proxy).

   * **Example:**

```
nginx

     keepalive_timeout 65s;

     client_body_timeout 10s;
```



  * **Log Analysis:** Look for errors related to connection timeouts, resets, or refused connections in the NGINX error log.



 * **B8: Describe basic SELinux use cases**

 * **SELinux Modes:**

  * **Enforcing:** SELinux actively enforces security policies and blocks actions that are not allowed.

  * **Permissive:** SELinux does not block actions but logs policy violations. Useful for troubleshooting SELinux issues.

  * **Disabled:** SELinux is turned off.

  * **Managing SELinux Policies:**

  * **`getenforce`:** Check the current SELinux mode.

  * **`setenforce`:** Temporarily change the SELinux mode (0 for permissive, 1 for enforcing).

  * **`semanage`:** Manage SELinux policies. For example, use `semanage port -a -t http_port_t -p tcp 8080` to allow NGINX to listen on port 8080. 



# **Section C: Troubleshoot TLS security settings**



 * **C1: Identify TLS connection errors**



  * **Log Analysis:** The NGINX error log (`error.log`) is your primary source of information for TLS connection errors. Look for entries related to SSL handshakes, certificate verification failures, protocol mismatches, or cipher suite issues.

  * **Example (error.log entry):** 

```

   2024/05/20 10:30:15 [crit] 12345#12345: *1 SSL_do_handshake() failed (SSL: error:1408F10B:SSL routines:ssl3_get_record:wrong version number) while SSL handshaking, client: 192.168.1.100, server: 0.0.0.0:443


# This indicates that the client is using an incompatible SSL/TLS version.
```

  * **`openssl s_client`:** This command-line tool allows you to test TLS connections from the client side. It can show details about the handshake process, certificate information, and any errors that occur.

   * **Example:**

```
bash

    openssl s_client -connect yourdomain.com:443

 # This will attempt to connect to `yourdomain.com` on port 443 and show the handshake details.
```


 * **C2: Describe how to troubleshoot invalid certificates**



 * **Certificate Validity:**

   * **Expiration Date:** Ensure that the certificate is still valid and has not expired. You can check the expiration date using `openssl x509 -in your_certificate.crt -noout -dates`.

   * **Issuer and Subject:** Verify that the issuer and subject of the certificate match what you expect. You can display this information using `openssl x509 -in your_certificate.crt -noout -subject -issuer`.



  * **Certificate Chain:**

   * **Root and Intermediate Certificates:** Ensure that the entire certificate chain is present and configured correctly in NGINX. This includes the root certificate, any intermediate certificates, and the server certificate itself.

   * **Order of Certificates:** In the NGINX `ssl_certificate` directive, list the certificates in the following order: server certificate, intermediate certificate(s), root certificate.



  * **Certificate Revocation:**

   * **OCSP (Online Certificate Status Protocol):** Check if the certificate has been revoked by the issuer using the OCSP protocol. You can use `openssl ocsp` to perform OCSP checks.

   * **CRL (Certificate Revocation List):** Alternatively, check the CRL provided by the issuer. The CRL is a list of revoked certificates.